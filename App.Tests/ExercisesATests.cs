// <copyright file="ExercisesATests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace App.Tests
{
    using App.Excercises;
    using Xunit;

    /// <summary>
    /// This Test Class contains the unit tests that should pass, calling <see cref="ExercisesA"/> class methods.
    /// </summary>
    public class ExercisesATests
    {
        #pragma warning disable SA1600 // Elements should be documented

        private readonly ExercisesA exercises;

        public ExercisesATests()
        {
            this.exercises = new ExercisesA();
        }

        [Theory]
        [InlineData(new object?[] { "Some basic string", "gnirts cisab emoS" })]
        [InlineData(new object?[] { "Icecream", "maercecI" })]
        [InlineData(new object?[] { "", "" })]
        [InlineData(new object?[] { null, null })]
        public void Invert_ReturnsExpectedResult(string input, string expectedResult)
        {
            string? result = this.exercises.Invert(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new object?[] { "This is a normal string", 5 })]
        [InlineData(new object?[] { "This string has spaces at the end     ", 7 })]
        [InlineData(new object?[] { "     This string has spaces at the beggining", 7 })]
        [InlineData(new object?[] { "      This string has spaces at both sides    ", 7 })]
        [InlineData(new object?[] { "This   string    has     spaces    between    words", 6 })]
        [InlineData(new object?[] { "", 0 })]
        [InlineData(new object?[] { null, 0 })]
        public void CountWords_ReturnsExpectedResult(string input, int? expectedResult)
        {
            int result = this.exercises.CountWords(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new object?[] { "This is a normal string", "normal" })]
        [InlineData(new object?[] { "I like Icecream", "Icecream" })]
        [InlineData(new object?[] { "Hello!", "Hello!" })]
        [InlineData(new object?[] { " ", "" })]
        [InlineData(new object?[] { null, null })]
        public void GetLongestWord_ReturnsExpectedResult(string input, string expectedResult)
        {
            string? result = this.exercises.GetLongestWord(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new object?[] { "Some basic string", "Sm bsc strng" })]
        [InlineData(new object?[] { "Pizza", "Pzz" })]
        [InlineData(new object?[] { "AAAAH!", "H!" })]
        [InlineData(new object?[] { "Zzz", "Zzz" })]
        [InlineData(new object?[] { " ", " " })]
        [InlineData(new object?[] { null, null })]
        public void Disemvowl_ReturnsExpectedResult(string input, string expectedResult)
        {
            string? result = this.exercises.Disemvowl(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new object[] { 19159283946654, 9 })]
        [InlineData(new object[] { 1, 1 })]
        [InlineData(new object[] { 99999, 9 })]
        [InlineData(new object[] { 0, 0 })]
        [InlineData(new object[] { 9546431, 5 })]
        [InlineData(new object[] { -9546431, 5 })]
        public void SumDigits_ReturnsExpectedResult(long input, int expectedResult)
        {
            int result = this.exercises.SumDigits(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new object?[] { "Some basic string", "SomeBasicString" })]
        [InlineData(new object?[] { "it's pizza time!", "It'sPizzaTime!" })]
        [InlineData(new object?[] { "heLLO WorLd", "HelloWorld" })]
        [InlineData(new object?[] { " ", "" })]
        [InlineData(new object?[] { null, null })]
        public void ToPascalCase_ReturnsExpectedResult(string input, string expectedResult)
        {
            string? result = this.exercises.ToPascalCase(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new object?[] { "SomeBasicString", "Some basic string" })]
        [InlineData(new object?[] { "It'sPizzaTime!", "It's pizza time!" })]
        [InlineData(new object?[] { "HelloWorld", "Hello world" })]
        [InlineData(new object?[] { " ", "" })]
        [InlineData(new object?[] { null, null })]
        public void BreakPascalCase_ReturnsExpectedResult(string input, string expectedResult)
        {
            string? result = this.exercises.BreakPascalCase(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new object?[] { "BasicString", "CbtjdTusjoh" })]
        [InlineData(new object?[] { "Hey Jude!", "Ifz Kvef!" })]
        [InlineData(new object?[] { "Zoom meeting", "Appn nffujoh" })]
        [InlineData(new object?[] { "1st place", "1tu qmbdf" })]
        [InlineData(new object?[] { " ", " " })]
        [InlineData(new object?[] { null, null })]
        public void MoveLetters_ReturnsExpectedResult(string input, string expectedResult)
        {
            string? result = this.exercises.MoveLetters(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new object?[] { "qwerty", "eqrtwy" })]
        [InlineData(new object?[] { "50 days of static", "05aacdfiosstty" })]
        [InlineData(new object?[] { " ", "" })]
        [InlineData(new object?[] { null, null })]
        public void OrderString_ReturnsExpectedResult(string input, string expectedResult)
        {
            string? result = this.exercises.OrderString(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new object?[] { "Hello there!", 'e' })]
        [InlineData(new object?[] { "I want pizza!", 'i' })]
        [InlineData(new object?[] { "Nope", 'n' })]
        [InlineData(new object?[] { "Year 1999", '9' })]
        [InlineData(new object?[] { "Let me think...", '.' })]
        [InlineData(new object?[] { null, null })]
        public void GetMostRepeatedChar_ReturnsExpectedResult(string input, char? expectedResult)
        {
            char? result = this.exercises.GetMostRepeatedChar(input);
            Assert.Equal(expectedResult, result.HasValue ? char.ToLower(result.Value) : result);
        }

        [Theory]
        [InlineData(new object?[] { "256", true })]
        [InlineData(new object?[] { "One", false })]
        [InlineData(new object?[] { "18th", false })]
        [InlineData(new object?[] { "18446744073709551616", true })]
        [InlineData(new object?[] { "1 ", true })]
        [InlineData(new object?[] { "19    92", false })]
        [InlineData(new object?[] { " ", false })]
        [InlineData(new object?[] { null, false })]
        public void IsNumeric_ReturnsExpectedResult(string input, bool expectedResult)
        {
            bool result = this.exercises.IsNumeric(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new object?[] { "256", "625", true })]
        [InlineData(new object?[] { "IceMan", "Cinema", true })]
        [InlineData(new object?[] { "William Shakespeare", "I am a weakish speller", true })]
        [InlineData(new object?[] { "Aaab", "baA", false })]
        [InlineData(new object?[] { null, null, false })]
        public void AreAnagrams_ReturnsExpectedResult(string inputA, string inputB, bool expectedResult)
        {
            bool result = this.exercises.AreAnagrams(inputA, inputB);
            Assert.Equal(expectedResult, result);
        }

        #pragma warning restore SA1600 // Elements should be documented
    }
}