# Exercises App

## Exercise scripts
Exercises scripts can be found on `App` project, inside `Exercises` folder.


The main goal is to implement your own solution for each method, the expected behaviour is described on the method's summary.

## Program.cs
This script has the main logic to run the Console App using exercises class methods.


Dont forget to update this line with your name so it can be displayed on the console screen

```csharp
public static readonly string StudentName = "PUT YOUR NAME HERE";
```

To run the console with a different exercise script update this line with the desired class:

``` csharp
MemberReader<ExerciseClass> reader = new ();
```

## Utilities scripts
Utilities scripts can be found on `Utilities` folder, this scripts will read the class used when instanciating the `MemberReader` class.

This class will find all public methods declared on the class and allow the user to call it by the console, sending the input parameters.

# Tests
This project runs unit tests using `xUnit` framework. It contains tests scripts to make sure the Exercises classes works as expected.

To run the tests go to `Tests -> Run All Tests`, or press `Ctrl + R, A`.