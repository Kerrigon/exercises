﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace App
{
    using App.Excercises;
    using App.Utilities;

    /// <summary>
    /// This class can be used to manually test and debug the progress. This class can be used on a live demo.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Student's name.
        /// </summary>
        public static readonly string StudentName = "PUT YOUR NAME HERE";

        /// <summary>
        /// Console App Main method.
        /// </summary>
        public static void Main()
        {
            MemberReader<ExercisesA> reader = new ();

            do
            {
                Run(reader);
            }
            while (true);
        }

        private static void Run<T>(MemberReader<T> reader)
            where T : class, new()
        {
            Console.Clear();
            Console.WriteLine(string.Format(MessageConstants.NameMessage, StudentName));
            (object? result, long ms) = reader.SelectAndInvokeMethod();
            string message = result is null ? MessageConstants.NoResultMessage : string.Format(MessageConstants.ResultMessage, result, ms);
            Console.WriteLine(message);
            Console.ReadKey();
        }
    }
}