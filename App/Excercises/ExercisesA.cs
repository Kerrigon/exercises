﻿// <copyright file="ExercisesA.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace App.Excercises
{
    /// <summary>
    /// This class contains the exercises to solve.
    /// </summary>
    public class ExercisesA
    {
        /// <summary>
        /// This method will invert the input string and return the result.
        /// Example: Input = "That will be 50 dollars.", Output = ".srallod 05 eb lliw tahT".
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>Inverted string.</returns>
        public string? Invert(string? input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will count the words in the input string and return the result.
        /// Example: Input = "Your friendly neighborhood Spiderman", Output = 4.
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>Word count.</returns>
        public int CountWords(string? input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will find the first longest word and return it.
        /// Example: Input = "We are the warriors that built this town", Output = "warriors".
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>First longest word.</returns>
        public string? GetLongestWord(string? input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will remove all the vowels from the input string and return the result.
        /// Example: Input = "Hello world!", Output = "Hll wrld!".
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>Disemvowled string.</returns>
        public string? Disemvowl(string? input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will sum all the digits of the input until there is only one digit result and return it.
        /// Example: Input = 92183, Output = 5. Because 9 + 2 + 1 + 8 + 3 = 23, and 2 + 3 = 5.
        /// </summary>
        /// <param name="input">Input number.</param>
        /// <returns>Sum of input digits.</returns>
        public int SumDigits(long input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will format the input string to PascalCase and return the result.
        /// Example: Input = "a noRmal string", Output = "ANormalString".
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>PascalCase result.</returns>
        public string? ToPascalCase(string? input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will add a space between the input words and return the result removeing capital letters.
        /// Example: Input = "MyPascalCaseExample", Output = "My pascal case example".
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>String result.</returns>
        public string? BreakPascalCase(string? input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will replace all letters with the following letter on the alphabet.
        /// Example: Input "Ab b c Z", Output = "Bc c d A". No numbers or symbols will be considered.
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>String result.</returns>
        public string? MoveLetters(string? input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will order alphabetically the input string (including numbers) ignoring spaces and return the result.
        /// Example: Input = "50 days of static", Output = "05aacdfiosstty".
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>String result.</returns>
        public string? OrderString(string? input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will find the first most repeated char in the input string ignoring spaces and casing and return it.
        /// Example: Input = "How do you turn this ON?", Output = 'o'.
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>Most repeated char.</returns>
        public char? GetMostRepeatedChar(string? input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will evaluate the input string and will return if it's is numeric.
        /// Example: Input = 42, Output = true.
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>Is numeric.</returns>
        public bool IsNumeric(string? input)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will compare the two input strings and return if they are anagrams. Anagram is a word, phrase, or name formed by rearranging the letters of another.
        /// Example: Input = "Cinema", "Iceman", Output = "true".
        /// </summary>
        /// <param name="inputA">Input string A.</param>
        /// <param name="inputB">Input string B.</param>
        /// <returns>Are anagrams.</returns>
        public bool AreAnagrams(string inputA, string inputB)
        {
            throw new NotImplementedException();
        }
    }
}
