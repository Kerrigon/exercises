﻿// <copyright file="MessageConstants.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace App.Utilities
{
    /// <summary>
    /// This static class contains the messages that will be used by the App.
    /// </summary>
    internal static class MessageConstants
    {
        /// <summary>
        /// Message that will be displayed on the top of the console.
        /// </summary>
        internal const string NameMessage = "{0}'s exercise solutions:\n\n";

        /// <summary>
        /// Format of the main menu, 0 being the index number and 1 being the method name.
        /// </summary>
        internal const string MenuFormat = "{0})\t{1}";

        /// <summary>
        /// Message that will be displayed when showing the method's result.
        /// </summary>
        internal const string ResultMessage = "Result: '{0}'.\nElapsed time during execution: {1} miliseconds.";

        /// <summary>
        /// Message that will be displayed when an exception is thrown.
        /// </summary>
        internal const string ErrorMessage = "Exception message: '{0}'";

        /// <summary>
        /// Message that will be displayed when showing the methods to the user.
        /// </summary>
        internal const string MethodsMessage = "{0} declared public methods detected on the instance:";

        /// <summary>
        /// Message that will be displayed when the input is not valid.
        /// </summary>
        internal const string InvalidInputMessage = "Invalid input.";

        /// <summary>
        /// Message that will be displayed when requesting the first parameter to the user.
        /// </summary>
        internal const string ParameterInputMessage = "Enter the {0} {1} parameter for the method '{2}':";

        /// <summary>
        /// Message that will be displayed when a method parameter type is not supported.
        /// </summary>
        internal const string TypeNotSupportedMessage = "Sorry, this parameter type ({0}) is not supported by console, a null value will be sent instead.";

        /// <summary>
        /// Message that will be displayed when the reader returns a null value.
        /// </summary>
        internal const string NoResultMessage = "No result was detected.";

        /// <summary>
        /// Message that will be displayed when a NotImplementedException is thrown.
        /// </summary>
        internal const string NotImplementedMessage = "Come on, you need to implement this exercise first >:v";
    }
}
