﻿// <copyright file="MemberReader.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace App.Utilities
{
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Reflection;

    /// <summary>
    /// This class is used to read the public declared methods of the instance class type and print them on the console,
    /// allowing the user to call a specific method using the console input.
    /// </summary>
    /// <typeparam name="T">Class type to read.</typeparam>
    internal class MemberReader<T>
        where T : class, new()
    {
        private readonly T instance;
        private readonly Type type;
        private readonly MethodInfo[] classMethods;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemberReader{T}"/> class.
        /// </summary>
        /// <param name="type">Class type to read.</param>
        internal MemberReader()
        {
            this.instance = Activator.CreateInstance<T>();
            this.type = typeof(T);
            this.classMethods = this.type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                .Where(method => !method.IsSpecialName).ToArray();
        }

        /// <summary>
        /// This method allows the user to call a method from the read class and returns the result, if any.
        /// </summary>
        /// <returns>Method result.</returns>
        internal (object? result, long ms) SelectAndInvokeMethod()
        {
            MethodInfo? methodInfo = SelectMethod(this.classMethods);

            if (methodInfo == null)
            {
                return default;
            }

            ParameterInfo[] parameters = methodInfo.GetParameters();
            IEnumerable<object?> parameterValues = GetParameterValues(methodInfo.Name, parameters);
            object?[] parameterValuesArray = parameterValues.ToArray();

            return InvokeMethod(this.instance, methodInfo, parameterValuesArray);
        }

        private static IEnumerable<object?> GetParameterValues(string methodName, ParameterInfo[] parameters)
        {
            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];
                Type parameterType = parameter.ParameterType;
                Console.WriteLine(string.Format(MessageConstants.ParameterInputMessage, parameter.ParameterType.Name, parameter.Name, methodName));
                string? methodInput = Console.ReadLine();
                TypeConverter converter = TypeDescriptor.GetConverter(parameterType);

                if (methodInput != null && converter.IsValid(methodInput))
                {
                    object? convertedValue = converter.ConvertFrom(methodInput);

                    yield return convertedValue;
                    continue;
                }

                Console.WriteLine(MessageConstants.InvalidInputMessage);
                yield return null;
            }
        }

        private static MethodInfo? SelectMethod(MethodInfo[] methods)
        {
            Console.WriteLine(string.Format(MessageConstants.MethodsMessage, methods.Length) + Environment.NewLine);

            if (methods.Length == 0)
            {
                return default;
            }

            for (int i = 0; i < methods.Length; i++)
            {
                Console.WriteLine(string.Format(MessageConstants.MenuFormat, i + 1, methods[i].Name));
            }

            string? input = Console.ReadLine();

            if (!int.TryParse(input, out int parsedInput) || parsedInput <= 0 || parsedInput > methods.Length)
            {
                Console.WriteLine(MessageConstants.InvalidInputMessage);
                return default;
            }

            return methods[parsedInput - 1];
        }

        private static (object? result, long ms) InvokeMethod(T instance, MethodInfo methodInfo, object?[] parameterValues)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            try
            {
                object? result = methodInfo.Invoke(instance, parameterValues);
                stopwatch.Stop();

                return (result, stopwatch.ElapsedMilliseconds);
            }
            catch (Exception ex) when (ex.InnerException is NotImplementedException)
            {
                Console.WriteLine(MessageConstants.NotImplementedMessage);
                stopwatch.Stop();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format(MessageConstants.ErrorMessage, ex.InnerException?.Message ?? ex.Message));
                stopwatch.Stop();
            }

            return default;
        }
    }
}
